## README

### How to use the code:
This python code is targeted to solve project Euler's first problem, which can be found [here.] (https://projecteuler.net/problem=1)

After cloning this repository in your computer, go to the terminal and type `python3 mult.py`
